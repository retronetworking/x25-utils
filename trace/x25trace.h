/* In listen.c */
void data_dump(unsigned char *, int, int);

/* In x25dump.c */
void x25_dump(unsigned char *, int, int);

#define X25DUMP_HEXDUMP 1 << 0	/* dump in hex format */
#define X25DUMP_DATA	1 << 1	/* dump contents of N_Data packets */
