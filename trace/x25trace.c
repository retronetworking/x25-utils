#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_ether.h>
#include <netinet/in.h>
#include <sys/ioctl.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <linux/x25.h>

#include "x25trace.h"

int main(int argc, char **argv)
{
	unsigned char buffer[1500];
	unsigned int size;
	int dump_flags = X25DUMP_DATA;
	int s;
	struct sockaddr sa;
	int asize;

	while ((s = getopt(argc, argv, "hd")) != -1) {
		switch (s) {
			case 'h':
				dump_flags ^= X25DUMP_HEXDUMP;
				break;
			case 'd':
				dump_flags &= ~X25DUMP_DATA;
				break;
			case ':':
			case '?':
				fprintf(stderr, "Usage: x25trace [-h] [-d]\n");
				return 1;
		}
	}

	if ((s = socket(AF_PACKET, SOCK_PACKET, htons(ETH_P_X25))) == -1) {
		perror("x25trace: socket");
		return 1;
	}
	
	for (;;) {
		asize = sizeof(sa);
	
		if ((size = recvfrom(s, buffer, sizeof(buffer), 0, &sa, &asize)) == -1) {
			perror("x25trace: recv");
			return 1;
		}
		
		switch (buffer[0]) {
			case 0x00:
				fprintf(stdout, "Port: %s\n", sa.sa_data);
				x25_dump(buffer + 1, size - 1, dump_flags);
				break;
			case 0x01:
				fprintf(stdout, "Port: %s - Connection Established\n", sa.sa_data);
				break;
			case 0x02:
				fprintf(stdout, "Port: %s - Connection Terminated\n", sa.sa_data);
				break;
			case 0x03:
				fprintf(stdout, "Port: %s - Connection Parameters\n", sa.sa_data);
				break;
			default:
				fprintf(stdout, "Port: %s - Unknown type 0x%02X\n", sa.sa_data, buffer[0]);
				break;
		}

		putc('\n', stdout);
		fflush(stdout);
	}
}

static void ascii_dump(unsigned char *data, int length)
{
	unsigned char c;
	int  i, j;

	for (i = 0; length > 0; i += 64) {
		fprintf(stdout, "%04X  ", i);
	
		for (j = 0; j < 64 && length > 0; j++) {
			c = *data++;
			length--;
			
			if ((c >= 0x20 && c < 0x7F) || c >= 0xA0)
				putc(c, stdout);
			else
				putc('.', stdout);
		}
		
		putc('\n', stdout);
	}
}

static void hex_dump(unsigned char *data, int length)
{
	int  i, j, length2;
	unsigned char c;
	char *data2;

	length2 = length;
	data2   = data;
	
	for (i = 0; length > 0; i += 16) {
		fprintf(stdout, "%04X  ", i);
	
		for (j = 0; j < 16; j++) {
			c = *data2++;
			length2--;
			
			if (length2 >= 0)
				printf("%2.2X ", c);
			else
				printf("   ");
		}

		printf(" | ");
				
		for (j = 0; j < 16 && length > 0; j++) {
 			c = *data++;
 			length--;
 			
			if (c >= 0x20 && c < 0x7F)
				putc(c, stdout);
			else
				putc('.', stdout);
		}
		
		putc('\n', stdout);
	}
}

void data_dump(unsigned char *data, int length, int dump_flags)
{
	if (dump_flags & X25DUMP_HEXDUMP)
		hex_dump(data, length);
	else
		ascii_dump(data, length);
}
