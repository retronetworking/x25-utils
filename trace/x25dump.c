/*
 * Copyright 1996 Jonathan Naylor G4KLX
 */
#include <stdio.h>
#include <string.h>
#include "x25trace.h"

#define	CALL_REQUEST			0x0B
#define	CALL_ACCEPTED			0x0F
#define	CLEAR_REQUEST			0x13
#define	CLEAR_CONFIRMATION		0x17
#define	INTERRUPT			0x23
#define	INTERRUPT_CONFIRMATION		0x27
#define	RESET_REQUEST			0x1B
#define	RESET_CONFIRMATION		0x1F
#define	RESTART_REQUEST			0xFB
#define	RESTART_CONFIRMATION		0xFF
#define	REGISTRATION_REQUEST		0xF3
#define	REGISTRATION_CONFIRMATION	0xF7
#define	DIAGNOSTIC			0xF1
#define	RR				0x01
#define	RNR				0x05
#define	REJ				0x09
#define	DATA				0x00

#define	QBIT				0x80
#define	DBIT				0x40
#define	MBIT				0x10

#define X25_FAC_CLASS_MASK      0xC0

#define X25_FAC_CLASS_A         0x00
#define X25_FAC_CLASS_B         0x40
#define X25_FAC_CLASS_C         0x80
#define X25_FAC_CLASS_D         0xC0

#define X25_FAC_REVERSE         0x01
#define X25_FAC_THROUGHPUT      0x02
#define X25_FAC_PACKET_SIZE     0x42
#define X25_FAC_WINDOW_SIZE     0x43

static int  x25_ntoa(unsigned char *, char *, char *, int);
static int  dump_facilities(unsigned char *, int);
static char *clear_code(unsigned char);
static char *reset_code(unsigned char);
static char *restart_code(unsigned char);

void x25_dump(unsigned char *data, int length, int dump_flags)
{
	char called[256], calling[256], *toa="";
	int len, A_bit, i;

	fprintf(stdout, "X.25: LCI %3.3X : ", 256*(data[0] & 0x0F) + data[1]);

	switch (data[2]) {
	case CALL_REQUEST:
		A_bit = data[0] & QBIT;
		if(A_bit) toa="[TOA/NPI-addr]";
		data   += 3;
		length -= 3;
		if( dump_flags & X25DUMP_HEXDUMP){
			printf("[%d]",length);
			for(i=0;i<length;i++){
				printf(" %2.2x", data[i]);
			}
			printf("\n               ");
		}
		len = x25_ntoa(data, called, calling, A_bit);
		if (len > 1)
			fprintf(stdout, "CALL REQUEST%s - %s -> %s\n",
				toa, calling, called);
		else
			fprintf(stdout, "CALL REQUEST%s\n",toa);
		data   += len;
		length -= len;
		len = dump_facilities(data, length);
		data   += len;
		length -= len;
		data_dump(data, length, 1);
		return;

	case CALL_ACCEPTED:
		A_bit = data[0] & QBIT;
		if(A_bit) toa="[TOA/NPI-addr]";
		data   += 3;
		length -= 3;
		len = x25_ntoa(data, called, calling,A_bit);
		if (len > 1)
			fprintf(stdout, "CALL ACCEPTED%s - %s -> %s\n",
				toa, calling, called);
		else
			fprintf(stdout, "CALL ACCEPTED\n");
		data   += len;
		length -= len;
		len = dump_facilities(data, length);
		data   += len;
		length -= len;
		data_dump(data, length, 1);
		return;

	case CLEAR_REQUEST:
		fprintf(stdout, "CLEAR REQUEST - Cause %s - Diag %d\n",
			clear_code(data[3]), data[4]);
		return;

	case CLEAR_CONFIRMATION:
		fprintf(stdout, "CLEAR CONFIRMATION\n");
		return;

	case DIAGNOSTIC:
		fprintf(stdout, "DIAGNOSTIC - Diag %d\n", data[3]);
		return;

	case INTERRUPT:
		fprintf(stdout, "INTERRUPT\n");
		data_dump(data + 3, length - 3, dump_flags);
		return;

	case INTERRUPT_CONFIRMATION:
		fprintf(stdout, "INTERRUPT CONFIRMATION\n");
		return;

	case RESET_REQUEST:
		fprintf(stdout, "RESET REQUEST - Cause %s - Diag %d\n",
			reset_code(data[3]), data[4]);
		return;

	case RESET_CONFIRMATION:
		fprintf(stdout, "RESET CONFIRMATION\n");
		return;
		
	case RESTART_REQUEST:
		fprintf(stdout, "RESTART REQUEST - Cause %s - Diag %d\n",
			restart_code(data[3]), data[4]);
		return;
		
	case RESTART_CONFIRMATION:
		fprintf(stdout, "RESTART CONFIRMATION\n");
		return;

	case REGISTRATION_REQUEST:
		fprintf(stdout, "REGISTRATION REQUEST\n");
		data_dump(data + 3, length - 3, 1);
		return;
		
	case REGISTRATION_CONFIRMATION:
		fprintf(stdout, "REGISTRATION CONFIRMATION\n");
		data_dump(data + 3, length - 3, 1);
		return;
	}

	if ((data[2] & 0x01) == DATA) {
		fprintf(stdout, "DATA R%d S%d %s%s%s\n",
			(data[2] >> 5) & 0x07, (data[2] >> 1) & 0x07,
			(data[0] & QBIT) ? "Q" : "",
			(data[0] & DBIT) ? "D" : "",
			(data[2] & MBIT) ? "M" : "");
		if(dump_flags & X25DUMP_DATA){
			data_dump(data + 3, length - 3, dump_flags);
		}
		return;
	}

	switch (data[2] & 0x1F) {
		case RR:
			fprintf(stdout, "RR R%d\n", (data[2] >> 5) & 0x07);
			return;
		case RNR:
			fprintf(stdout, "RNR R%d\n", (data[2] >> 5) & 0x07);
			return;
		case REJ:
			fprintf(stdout, "REJ R%d\n", (data[2] >> 5) & 0x07);
			return;
	}

	fprintf(stdout, "UNKNOWN\n");
	data_dump(data, length, 1);
}

static char *clear_code(unsigned char code)
{
	static char buffer[25];

	if (code == 0x00 || (code & 0x80) == 0x80)
		return "DTE Originated";
	if (code == 0x01)
		return "Number Busy";
	if (code == 0x09)
		return "Out Of Order";
	if (code == 0x11)
		return "Remote Procedure Error";
	if (code == 0x19)
		return "Reverse Charging Acceptance Not Subscribed";
	if (code == 0x21)	
		return "Incompatible Destination";
	if (code == 0x29)
		return "Fast Select Acceptance Not Subscribed";
	if (code == 0x39)
		return "Destination Absent";
	if (code == 0x03)
		return "Invalid Facility Requested";
	if (code == 0x0B)
		return "Access Barred";
	if (code == 0x13)
		return "Local Procedure Error";
	if (code == 0x05)
		return "Network Congestion";
	if (code == 0x0D)
		return "Not Obtainable";
	if (code == 0x15)
		return "RPOA Out Of Order";
	
	sprintf(buffer, "Unknown %02X", code);
	
	return buffer;
}

static char *reset_code(unsigned char code)
{
	static char buffer[25];

	if (code == 0x00 || (code & 0x80) == 0x80)
		return "DTE Originated";
	if (code == 0x03)
		return "Remote Procedure Error";
	if (code == 0x11)	
		return "Incompatible Destination";
	if (code == 0x05)
		return "Local Procedure Error";
	if (code == 0x07)
		return "Network Congestion";
	
	sprintf(buffer, "Unknown %02X", code);
	
	return buffer;
}

static char *restart_code(unsigned char code)
{
	static char buffer[25];

	if (code == 0x00 || (code & 0x80) == 0x80)
		return "DTE Originated";
	if (code == 0x01)
		return "Local Procedure Error";
	if (code == 0x03)
		return "Network Congestion";
	if (code == 0x07)
		return "Network Operational";
	
	sprintf(buffer, "Unknown %02X", code);
	
	return buffer;
}

static int x25_ntoa(unsigned char *p, char *called, char *calling, int A_bit)
{
	int called_len, calling_len;
	int i;

	if(A_bit == 0){
		called_len  = (*p >> 0) & 0x0F;
		calling_len = (*p >> 4) & 0x0F;
	} else {
		called_len  = *p++;
		calling_len = *p;
	}
	p++;

	for (i = 0; i < (called_len + calling_len); i++) {
		if (i < called_len) {
			if (i % 2 != 0) {
				*called++ = ((*p >> 0) & 0x0F) + '0';
				p++;
			} else {
				*called++ = ((*p >> 4) & 0x0F) + '0';
			}
		} else {
			if (i % 2 != 0) {
				*calling++ = ((*p >> 0) & 0x0F) + '0';
				p++;
			} else {
				*calling++ = ((*p >> 4) & 0x0F) + '0';
			}
		}
	}

	*called  = '\0';
	*calling = '\0';

	return 1 + (called_len + calling_len + 1) / 2;
}

static int dump_facilities(unsigned char *data, int length)
{
	unsigned char *p = data;
	unsigned int len;

	if (length <= 0)
		return 0;

	len = *p++;

	while (len > 0) {
		switch (*p & X25_FAC_CLASS_MASK) {
			case X25_FAC_CLASS_A:
				switch (*p) {
					case X25_FAC_REVERSE:
						fprintf(stdout, "Reverse Charging: %d\n", p[1] & 0x01);
						break;
					case X25_FAC_THROUGHPUT:
						fprintf(stdout, "Throughput:       %02X\n", p[1]);
						break;
					default:
						fprintf(stdout, "Unknown facility %02X, value %02X\n", p[0], p[1]);
						break;
				}
				p   += 2;
				len -= 2;
				break;
				
			case X25_FAC_CLASS_B:
				switch (*p) {
					case X25_FAC_PACKET_SIZE:
						fprintf(stdout, "Packet Size:     %02X %02X\n", p[1], p[2]);
						break;
					case X25_FAC_WINDOW_SIZE:
						fprintf(stdout, "Window Size:     %2d %2d\n", p[1], p[2]);
						break;
					default:
						fprintf(stdout, "Unknown facility %02X, values %02X, %02X\n", p[0], p[1], p[2]);
						break;
				}
				p   += 3;
				len -= 3;
				break;

			case X25_FAC_CLASS_C:
				fprintf(stdout, "Uunknown facility %02X, values %02X, %02X, %02X\n", p[0], p[1], p[2], p[3]);
				p   += 4;
				len -= 4;
				break;
				
			case X25_FAC_CLASS_D:
				fprintf(stdout, "Unknown facility %02X, length %d, values %02X, %02X, %02X, %02X\n", p[0], p[1], p[2], p[3], p[4], p[5]);
				p   += p[1] + 2;
				len -= p[1] + 2;
				break;
		}
	}

	return p - data;
}
