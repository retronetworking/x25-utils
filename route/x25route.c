#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <linux/x25.h>

void add_route(int s, char *addr, char *dev)
{
	struct x25_route_struct x25_route;
	char *p;
	int n;

	if ((p = strchr(addr, '/')) != NULL) {
		*p = '\0';
		n = atoi(p + 1);
	} else {
		n = strlen(addr);
	}

	if (strlen(addr) < 1 || strlen(addr) > 15 || n > strlen(addr)) {
		fprintf(stderr, "x25route: invalid routing address\n");
		close(s);
		exit(1);
	}

	strcpy(x25_route.address.x25_addr, addr);
	x25_route.sigdigits = n;
	strcpy(x25_route.device, dev);

	if (ioctl(s, SIOCADDRT, &x25_route) == -1) {
		perror("x25route: SIOCADDRT");
		close(s);
		exit(1);
	}
}

void del_route(int s, char *addr, char *dev)
{
	struct x25_route_struct x25_route;
	char *p;
	int n;

	if ((p = strchr(addr, '/')) != NULL) {
		*p = '\0';
		n = atoi(p + 1);
	} else {
		n = strlen(addr);
	}

	if (strlen(addr) < 1 || strlen(addr) > 15 || n > strlen(addr)) {
		fprintf(stderr, "x25route: invalid routing address\n");
		close(s);
		exit(1);
	}

	strcpy(x25_route.address.x25_addr, addr);
	x25_route.sigdigits = n;
	strcpy(x25_route.device, dev);

	if (ioctl(s, SIOCDELRT, &x25_route) == -1) {
		perror("x25route: SIOCADDRT");
		close(s);
		exit(1);
	}
}

int main(int argc, char *argv[])
{
	int s;
	
	if (argc < 4) {
		fprintf(stderr, "usage: x25route add|del <address>[/<mask>] <dev>\n");
		return 1;
	}

	if ((s = socket(AF_X25, SOCK_SEQPACKET, 0)) < 0) {
		perror("x25route: socket");
		return 1;
	}
	
	if (strcmp(argv[1], "add") == 0) {
		add_route(s, argv[2], argv[3]);
		close(s);
		return 0;
	}

	if (strcmp(argv[1], "del") == 0) {
		del_route(s, argv[2], argv[3]);
		close(s);
		return 0;
	}

	fprintf(stderr, "usage: x25route add|del <address>[/<mask>] <dev>\n");
	
	close(s);
	
	return 1;
}
